import pandas as pd
import numpy as np
import joblib
import logging
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score,classification_report



logging.basicConfig(filename='log/model.log',format='%(asctime)s %(levelname)s:%(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

data_features = pd.read_csv('10genres_original.csv')
label_dict = ['blues','classical','country','disco','hiphop','jazz','metal','pop','reggae','rock']
data_features['label'] = data_features['songname'].apply(lambda x: (x[:-3].split('.')[0]),label_dict)



#extracting dependent and independent variables

x= data_features.iloc[:, 1:-1].values
y= data_features['label'].tolist() 
logging.info('Extracted dependent and independent variables')


#splitting into train and test set
x_train, x_test, y_train, y_test= train_test_split(x, y, test_size= 0.25, random_state=0)
logging.info('Spliitng into train and test set')  
  


rf_classifier = RandomForestClassifier(n_estimators=500,random_state=5, min_samples_split=6, max_depth=100)
rf_classifier.fit(x_train, y_train)
pred_probs = rf_classifier.predict(x_test)
logging.info('Trained model')
logging.info('Test Accuracy predicted')


joblib.dump(rf_classifier,"model.pkl")
logging.info('Model dumped')

