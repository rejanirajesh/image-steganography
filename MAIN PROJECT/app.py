from project import app, db
from flask import render_template, redirect, request, url_for, flash, abort, session
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from project.models import User
from project.forms import LoginForm
from flask_sqlalchemy import SQLAlchemy
from project.genre_prediction import *
import os
import logging


logging.basicConfig(filename='log/frame.log',
                    format='%(asctime)s %(levelname)s:%(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

uploads_dir = os.path.join(app.instance_path, 'UPLOADS')
# os.makedirs(uploads_dir)


def validate(username,password):
    user = User.query.filter_by(username=username).first()
    if user and check_password_hash(user.pass_hash, password):
            # session[username] = True
            app.logger.info('%s logged in successfully', user.username)
            flag=1
            return flag
    else:
            flag=0
            return flag


@app.before_first_request
def create_all():
    db.create_all()


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/output', methods=['POST'])
def output():
    if request.method == 'POST':
        f = request.files['chooseFile']
        os.path.join(uploads_dir, secure_filename(f.filename))
        f.save(os.path.join(uploads_dir, secure_filename(f.filename)))
        Predicted_genre = Main_Feature().genre_output()
        return render_template('output.html',value=Predicted_genre)


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    if "username" in session:
        session.pop('email', None)
        logging.info("Logged out")
        flash('You have logged out successfully,{user}', 'success')
        return redirect(url_for('login'))

    logout_user()
    return redirect(url_for('login'))


@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        username = request.form['email']
        password = request.form['password']
        session['username'] = request.form['email']

        if not (username and password):
            flash("Username or Password cannot be empty.")
            return redirect(url_for('login'))
        else:
            username = username.strip()
            password = password.strip()

        if (validate(username,password) == 1):
            logging.info("Successful log-in")
            return redirect(url_for('upload', username=username))            
        else:
            flash("Invalid username or password.")
            logging.info("Log-in failure")
    return render_template("home.html")


@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == "POST":
        username = request.form['email']
        password = request.form['password']

        if not (username and password):
            flash("Username or Password cannot be empty")
            return redirect(url_for('register'))
        else:
            username = username.strip()
            password = password.strip()

        # Returns salted pwd hash in format : method$salt$hashedvalue
        hashed_pwd = generate_password_hash(password, 'sha256')

        new_user = User(username=username, pass_hash=hashed_pwd)
        db.session.add(new_user)

        try:
            db.session.commit()
        except:
            flash("Username {u} is not available.".format(u=username))
            return redirect(url_for('register'))

        flash("User account has been created.")
        return redirect(url_for('login'))

    return render_template("register.html")


if __name__ == '__main__':
    app.run(port=5000, debug=True)

   