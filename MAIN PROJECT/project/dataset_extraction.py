import librosa
import scipy
import joblib
import os
import logging
import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join


logging.basicConfig(filename='log/actvate.log',format='%(asctime)s %(levelname)s:%(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

class FeatureExtractor:


    def __init__(self):
        self.data = None
    
    def extract(self,path):
        try:
            self.data = []
            for f in os.listdir(path):
                file_name = os.path.join(path, f)
                if os.path.isfile(file_name):
                    features = []
                        
                    y, sr = librosa.load(file_name,duration=30)
                    zcr = librosa.feature.zero_crossing_rate(y)
                    rmse = librosa.feature.rms(y=y)
                    tempo = librosa.beat.tempo(y=y, sr=sr)
                    spectral_centroids = librosa.feature.spectral_centroid(y=y, sr=sr)  
                    spectral_bandwidth_2 = librosa.feature.spectral_bandwidth(y=y, sr=sr, p=2)
                    spectral_bandwidth_3 = librosa.feature.spectral_bandwidth(y=y, sr=sr, p=3)
                    spectral_bandwidth_4 = librosa.feature.spectral_bandwidth(y=y, sr=sr, p=4) 
                    spectral_contrast = librosa.feature.spectral_contrast(y, sr=sr)
                    spectral_rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr) 
                    mfcc = librosa.feature.mfcc(y=y, sr=sr)
                    chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
                        
                    
                    
                    features.append(f)
                    features.append(np.mean(abs(y)))
                    features.append(np.std(y))
                    features.append(scipy.stats.skew(abs(y)))
                    features.append(scipy.stats.kurtosis(y))
                    features.append(np.mean(zcr))
                    features.append(np.std(zcr))
                    features.append(np.mean(rmse))
                    features.append(np.std(rmse))
                    features.extend(tempo)
                    features.append(np.mean(spectral_centroids))
                    features.append(np.std(spectral_centroids))
                    features.append(np.mean(spectral_bandwidth_2))
                    features.append(np.std(spectral_bandwidth_2))
                    features.append(np.mean(spectral_bandwidth_3))
                    features.append(np.std(spectral_bandwidth_3))
                    features.append(np.mean(spectral_bandwidth_4))
                    features.append(np.std(spectral_bandwidth_4))
                    
                    for coefficient in spectral_contrast:
                        features.append(np.mean(coefficient))
                        features.append(np.std(coefficient))

                    features.append(np.mean(spectral_rolloff))
                    features.append(np.std(spectral_rolloff))

                    for coefficient in mfcc:
                        features.append(np.mean(coefficient))
                        features.append(np.std(coefficient))
                    
                    for coefficient in chroma_stft:
                        features.append(np.mean(coefficient))
                        features.append(np.std(coefficient))



                    self.data.append(features)

                    #os.remove(file_name)
                   
        except:
            logging.error('Encountered an error')
            pass
        logging.info('Features being extracted')



    def get_data(self):
        return self.data



    
# main ()
extractor = FeatureExtractor()
extractor.extract('genres_original/')



heading = [['songname','signal_mean', 'signal_std', 'signal_skew', 'signal_kurtosis', 
            'zcr_mean', 'zcr_std', 'rmse_mean', 'rmse_std', 'tempo',
            'spectral_centroid_mean', 'spectral_centroid_std',
            'spectral_bandwidth_2_mean', 'spectral_bandwidth_2_std',
            'spectral_bandwidth_3_mean', 'spectral_bandwidth_3_std',
            'spectral_bandwidth_4_mean', 'spectral_bandwidth_4_std']+\
            ['spectral_contrast_' + str(i+1) + '_mean' for i in range(7)] + \
            ['spectral_contrast_' + str(i+1) + '_std' for i in range(7)] + \
            ['spectral_rolloff_mean', 'spectral_rolloff_std']+\
            ['mfccs_' + str(i+1) + '_mean' for i in range(20)] + \
            ['mfccs_' + str(i+1) + '_std' for i in range(20)] + \
            ['chroma_stft_' + str(i+1) + '_mean' for i in range(12)] + \
            ['chroma_stft_' + str(i+1) + '_std' for i in range(12)]]



        
df = pd.DataFrame(extractor.get_data(), columns=heading)
#df =df.round(decimals=3)
df.to_csv('10genres_original.csv')
logging.info('Feature Extraction Successfull')












