from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField,BooleanField
from wtforms.validators import DataRequired, Email, EqualTo, Length
from wtforms import ValidationError
from project.models import User


class LoginForm(FlaskForm):

    email = StringField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    submit = SubmitField('submit')


class RegistrationForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(), Email(message='Enter a valid email.')])
    password = PasswordField('password', validators=[DataRequired(),Length(min=6, message='Select a stronger password.')])

