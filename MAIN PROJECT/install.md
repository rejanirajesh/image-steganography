# REQUIREMENTS 

### Front-end :  

        HTML  
        Jinja2 template  
        Bootstrap==4.3.1  
        A web browser( Google chrome etc .  )  

### Back-end :  

        python3.7 version
        Sqlite3 DB browser(directly from internet source)  
        Werkzeug==1.0.1  
        alembic==1.4.3  
        decorator==4.4.2  
        email-validator==1.1.2  
        Flask-Login==0.5.0  
        Flask-Migrate==2.5.3  
        Flask-Session==0.3.2  
        Flask-SQLAlchemy==2.4.4  
        Flask-WTF==0.14.3  
        Jinja2==2.11.2  
        SQLAlchemy==1.3.20  
        WTForms==2.3.3  
        pysqlite3==0.4.3  
        
        For genre prediction :  
        ------------------------
        joblib==0.17.0  
        librosa== 0.8.0  
        numpy==1.19.4  
        pandas==1.1.4  
        scikit-learn== 0.23.2  
        scipy==1.5.3  
        sklearn==0.0

# INSTRUCTIONS  

    1. In the terminal (editor), run the command :  

        • Virtual-environment(optional,to avoid clashes with other files)  

            --------------------------------------------------------
             virtual envname (creating the environment)      
             source env/bin/activate (to activate environment)  
            --------------------------------------------------------
       
        • Run the file app.py using 𝗽𝘆𝘁𝗵𝗼𝗻𝟯 𝗮𝗽𝗽.𝗽𝘆 to view the flask project
    
    2. Open the web browser, go to localhost:(port number)  

    3. Now , you get the project running and you can upload the .wav(only) audio files to get the predicted genre  

# PROJECT AT A GLANCE
        
![](https://gitlab.com/rejanirajesh/image-steganography/-/raw/master/MAIN%20PROJECT/project.gif)

            
