from PIL import Image
import sys


def main():
    # Main Function
    S = Steganography()
    user_input = 'y'
    while user_input == 'y':
        choice = int(input("-- Image Steganography --\n1. Encode \n2. Decode\n"))
        if choice == 1:
            S.encode_input()
        elif choice == 2:
            print("Decoded Word --> " + S.decode_input())
        else:
            print("invalidinput")
        user_input = input('Do you want to continue? ( y or n ) ')


class Steganography:

    def encode_input(self):           # input function for the main encoding program
        print('\t \t \t ENCODING ')
        print('---- ( Enter the image names with extension ) ----')
        img = input("Image name : ")
        image = Image.open(img, 'r')
        new_img = image.copy()
        data = input("Enter the data to be encoded : ")
        if len(data) == 0:
            print("Invalid data")
            sys.exit()
        self.encode(data, new_img)
        new_name = input("Name of new image : ")
        new_img.save(new_name)

    def encode(self, data, new_img):     # encoding main program
        width = new_img.size[0]
        (x_cord, y_cord) = (0, 0)
        for pixel in self.modified_Pix(new_img.getdata(), data):
            new_img.putpixel((x_cord, y_cord), pixel)
            if x_cord == width - 1:
                x_cord = 0
                y_cord += 1
            else:
                x_cord += 1

    def modified_Pix(self, pix, data):
        bin_list = self.bin_data(data)
        length = len(bin_list)
        obj_value = iter(pix)
        for i in range(length):
            pix = [value for value in next(obj_value)[:3] + next(obj_value)[:3] + next(obj_value)[:3]]
            # retrieves the first 3 pixels
            for j in range(0, 8):
                if bin_list[i][j] == '0' and pix[j] % 2 != 0:  # corresponding to 0, should be even
                    if pix[j] != 255:
                        pix[j] += 1
                    else:
                        pix[j] -= 1
                elif bin_list[i][j] == '1' and pix[j] % 2 == 0:  # corresponding to 1, should be odd
                    pix[j] += 1
            if i == length - 1:         # to check if more data has to be read, if i has reached the last pixel
                if pix[-1] % 2 == 0:
                    if pix[-1] != 0:
                        pix[-1] -= 1
                    else:
                        pix[-1] += 1
            else:
                if pix[-1] % 2 != 0:
                    pix[-1] -= 1
            pix = tuple(pix)
            yield pix[0:3]
            yield pix[3:6]
            yield pix[6:9]

    # convert a number to binary format
    def bin_data(self, data):
        binary = []

        for i in data:
            binary.append(format(ord(i), '08b'))
        return binary

    def decode_input(self):
        print('\t \t \t DECODING ')
        img = input("Enter image name(with extension) : ")
        image = Image.open(img, 'r')
        word = self.decode(image)
        return word

    def decode(self, image):
        data = ''
        img_data = iter(image.getdata())
        while True:
            pixels = [value for value in next(img_data)[:3] + next(img_data)[:3] + next(img_data)[:3]]
            string = ''
            for i in pixels[:8]:
                if i % 2 == 0:
                    string += '0'
                else:
                    string += '1'
            data += chr(int(string, 2))
            if pixels[-1] % 2 != 0:
                return data


# Driver Code
if __name__ == '__main__':
    # Calling main function
    main()
