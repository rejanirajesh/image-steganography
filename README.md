# Methodology:
The genre of a given audio file is predicted, implemented in web  

# Team members

1.Rejani Rajesh(43)  
2.Rithika Liz Bobby(45)  
3.Roniya Varghese(46)  
4.Rose Stombel (47)  

# Individual Contribution:
https://drive.google.com/drive/folders/1HarOV2inkbzCOfSjlkBJDl40OmcGs1vh

# Project at a glance:

![](https://gitlab.com/rejanirajesh/image-steganography/-/raw/master/MAIN%20PROJECT/project.gif)
